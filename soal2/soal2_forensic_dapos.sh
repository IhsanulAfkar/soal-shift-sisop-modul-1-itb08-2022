#!/bin/bash

#2a
[ -d forensic_log_website_daffainfo_log ] || mkdir forensic_log_website_daffainfo_log


#2b
rata2(){
	msg="$1"
	rata_msg="$msg"
	echo "$rata_msg" > forensic_log_website_daffainfo_log/ratarata.txt
}

info=$(awk -F: '
	NR==2{printf "%d %d %d\n", $3, $4, $5}
	END{printf "%d %d %d %d", $3, $4, $5, NR-1}
' log_website_daffainfo.log)
arr=(${info//:/ })

req="${arr[6]}"

jam=$(($((${arr[3]}-1))-${arr[0]}))
menit=$(($((${arr[4]+59}))-${arr[1]}))
detik=$(($((${arr[5]+60}))-${arr[2]}))
total_jam=$(($(($detik/3600))+$(($menit/60))+$jam))

ratarata=$(($req/$total_jam))

msg="Rata-rata serangan adalah sebanyak $ratarata requests per jam"
rata2 "$msg"

#2c
info=$(awk -F: '
	NR!=1{printf "%s\n", $1}
' log_website_daffainfo.log)
arr=(${info//'"'/ })

for i in ${arr[@]}
do
	echo $i >> temp_log.txt
done
$(awk '
	{if(arr[$0]++ >= max) 
		max = arr[$0]}
	END{for(i in arr) if(max==arr[i]) 
		printf "IP yang paling banyak mengakses server adalah: %s sebanyak %d requests\n\n",i,arr[i]}
' temp_log.txt > forensic_log_website_daffainfo_log/result.txt)
eval "rm temp_log.txt"

#2d
$(awk '
	/curl/ {n++} 
	END{printf "Ada %d requests yang menggunakan curl sebagai user-agent\n\n",n}
' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt)

#2e
$(awk -F: '
	{if($3=="02")
		printf "%s\n",$1}
' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt)
