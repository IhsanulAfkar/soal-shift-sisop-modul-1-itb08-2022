# soal-shift-sisop-modul-1-ITB08-2022

### <b> Anggota Kelompok: </b>
##### 1. 5027201001 - Najwa Amelia Qorry 'Aina
##### 2. 5027201024 - Muhammad Ihsanul Afkar
##### 3. 5027201072 - Shafira Khaerunnisa Latif
---

## Soal 1
Pada suatu hari, Han dan teman-temannya diberikan tugas untuk mencari foto. Namun, karena laptop teman-temannya rusak ternyata tidak bisa dipakai karena rusak, Han dengan senang hati memperbolehkan teman-temannya untuk meminjam laptopnya. Untuk mempermudah pekerjaan mereka, Han membuat sebuah program.

## Penyelesaian soal 1

### 1a
- `Nomor 1a` Membuat file ./users/user.txt, script register.sh untuk registrasi, dan main.sh untuk sistem login <br>

![ss_file1a.png](images/ss_file1a.png)

### 1b
- `Nomor 1b` Validasi requirement password dan username di register.sh <br>
    - Validasi terdapat pada fungsi `validate()` yang mengambil 2 parameter yaitu `username` dan `password`
    ```shell
    #1b
    validate(){
    username="$1"
    password="$2"
    #....
    }
    ```
    Adapun untuk setiap persyaratan password dan username adalah sebagai berikut <br>
    1. Panjang password lebih atau sama dengan 8 karakter <br>
        ```shell
        if [ ${#password} -lt 8 ]
        then
            echo $invalid
            return 0
        fi  
        ```
        `-lt` adalah command “lower than” dan sintaks `${#password}` adalah mengembalikan nilai jumlah dari karakter dari string password. Jika ternyata kurang dari 8 karakter, maka akan masuk if dan mencetak invalid sekaligus keluar dari function validate yang berarti password tidak memenuhi syarat.

    2. Memiliki minimal 1 huruf kapital dan 1 huruf kecil dan Alphanumeric <br>
        ```shell
        number=0
        uppercase=0
        lowercase=0
        for ((i=0; i<${#password}; i=i+1))
        do
            c="${password:$i:1}"
            if [[ "$c" =~ [A-Z] ]]
            then
                    ((uppercase++))
            elif [[ "$c" =~ [a-z] ]]
            then
                    ((lowercase++))
            elif [[ "$c" =~ [0-9] ]]
            then
                    ((number++))
            fi
        done
        if [ $uppercase == 0 ]
        then
            echo $invalid
            return 0
        elif [ $lowercase == 0 ]
        then
            echo $invalid
            return 0
        elif [ $number == 0 ]
        then
            echo $invalid
            return 0
        fi
        ```
        Pertama, akan dibuat 3 variabel untuk menghitung berapa jumlah dari angka, huruf kapital, dan huruf kecil. Setelah itu, akan dilakukan `loop` untuk setiap karakter dari `password`. Untuk sintaks` ${password:$i:1}` adalah mengambil karakter dari indeks `$i` sebanyak 1 karakter.

        Didalam if, akan di cek satu persatu karakter tersebut menggunakan nested if dengan parameter komparasi adalah regex (regular expression). Di dalam nested if tersebut, increment variabel sesuai regex pada if tersebut, `uppercase` untuk kapital, `lowercase` untuk huruf kecil, dan `number` untuk angka.

        Setelah keluar dari loop, akan di cek nilai dari `uppercase`, `lowercase`, dan `number`. Apabila salah satu dari variabel tersebut bernilai 0, maka akan mencetak `invalid` dan keluar dari fungsi `validate()`.
    3. Password tidak boleh sama dengan username
        ```shell
        if [ $password == $username ]
            then
                echo $invalid
                return 0
        fi  
        ```
        Hanya membuat if dengan meletakkan `username` dan `password` lalu di komparasi menggunakan `==`. Jika sama, maka akan masuk if lalu mencetak invalid dan keluar dari fungsi `validate()`.
    > Berikut adalah gambar contoh ketika user menggunakan invalid username atau password untuk register (tidak sesuai dengan requirement di atas):
![ss_error1b.png](images/ss_error1b.png)

### 1c
- `Nomor 1c` Setiap percobaan login dan register akan tercatat pada log.txt sesuai format <br>
    Fungsi log() untuk mencetak message kedalam log.txt
    ```shell
    log(){
        msg="$1"
        log_msg="$log_date $msg"
        echo "$log_msg" >> log.txt
    }
    ```
    Untuk detail setiap kasus akan dijelaskan sebagai berikut:
    - Register dengan username yang sudah terdaftar <br>
        ```shell
        check=$(awk -v pat="$username" '$1~pat{print $1}' users/user.txt)
        if [ -n "$check" ]
        then
            echo $invalid
            msg="REGISTER: ERROR User already exists"
            log "$msg"
            return 0
        fi
        ```
        Terdapat fungsi awk untuk mengambil data dari `users/user.txt` dengan pattern `username`. `[ -n “$check” ]`bertujuan untuk mengecek apakah `check` memiliki nilai, jika iya, maka akan masuk kedalam if lalu mencetak `invalid` setelah itu menjalankan fungsi log dengan parameter msg `REGISTER: ERROR User already exists`. 
    - Register berhasil <br>
        Pada fungsi `validate()`, di baris terakhir terdapat kode:
        ```shell
        regist "$username" "$password"
        ```
        Yang berarti, setelah `username` dan `password` tidak menyalahi aturan, maka panggil fungsi `regist()` dengan parameter `username` dan `password`. 
        ```shell
        regist(){
            echo "$1 $2" >> users/user.txt
            log "INFO User $1 registered successfully"
        }
        ```
        Pada fungsi `regist()`, baris pertama yaitu menyimpan `username` dan `password` ke dalam `users/user.txt`. Setelah itu menjalankan fungsi `log()` dengan parameter `INFO User $1 registered successfully` dengan `$1` adalah `username`.
    - Salah password ketika user mencoba login dan user telah berhasil login <br>
        Pada main.sh, terdapat fungsi `validate()` seperti berikut
        ```shell
        validate(){
            username="$1"
            password="$2"
            # Check if exist or not
            check=$(awk -v pat="$username" '$1~pat{print $2}' users/user.txt)
            if [ -z "$check" ]
            then
                echo $invalid
                return 0
            fi
            # Check password
            if [ "$check" != "$password" ]
            then
                echo $invalid
                msg="LOGIN: ERROR Failed login attempt on user $username"
                log "$msg"
                return 0
            fi
            # Logged in
            msg="LOGIN: INFO User $username logged in"
            log "$msg"
            login "$username" "$password"
        }
        ```
        Fungsi tersebut bertujuan untuk mem-validasi `username` dan `password` ketika akan login. Untuk log ketika password user salah ada di bagian `if [ “$check” != “$password” ]` yang dimana `check` bernilai dari fungsi awk yang mengambil `password` jika terdapat `username` yang sama di `users/user.txt`. Adapun msg log adalah `LOGIN: ERROR Failed login attempt on user $username`. 
        Untuk bagian user berhasil login ada pada bagian akhir dari fungsi `validate()` dengan msg log `LOGIN: INFO User $username logged in`. 
    > Berikut adalah contoh tampilan users/user.txt dan log.txt

![ss_users1.png](images/ss_users1.png)
![ss_log1.png](images/ss_log1.png)
- Keterangan: 
    - Line 1 : output ketika user mencoba register dengan username yang sudah terdaftar.
    - Line 2 : output ketika user berhasil melakukan register.
    - Line 3 : output ketika user berhasil login.
    - Line 4 : output ketika user gagal login karena password salah.

### 1d
- `Nomor 1d` Command-command ketika user berhasil login <br>
    Terdapat 2 command, yaitu `dl x` dan `att` dimana `dl x` adalah untuk mendownload gambar dari [loremflickr](https://loremflickr.com/320/240) sebanyak `x` kali dan `att` adalah untuk menampilkan percobaan login user baik yang gagal maupun berhasil.
    ```shell
    login(){
    username="$1"
    password="$2"
    while [ 1 ]
    do
        echo "Enter x/X to exit (or ^C)"
        read -p "Enter command: " input
        echo -e "\n"
        IFS=' '
        read -ra Arr <<< "$input"
        command="${Arr[0]}"
        case "$command" in
            "dl")
                num="${Arr[1]}"
                init_num=0;
                dir_date=$(date '+%Y-%m-%d')
                path_dir=$dir_date"_$username"
                FILE=$path_dir".zip"
                if [[ -f "$FILE" ]]
                then
                    eval "unzip -P $password $FILE"
                   
                    init_num=$(eval "ls $path_dir/ | wc -l")
                else
                    eval "mkdir $path_dir"
                fi
                total=$((init_num+num))
                for ((i=init_num+1; i<=total; i++))
                do
                    img_path=$path_dir"/PIC_$i"
                    wget_msg="wget -O $img_path https://loremflickr.com/320/240"
                    eval $wget_msg
                done
                zip_msg="zip -r -P $password $path_dir $path_dir"
                eval $zip_msg
                eval "rm -rf $path_dir"
            ;;
            "att")
                check_failed=$(awk -v pat="$username" '$NF~pat{++n} END {print n}' log.txt)
                check_succeed=$(awk -v pat="$username" '$6~pat{++n} END {print n}' log.txt)
                check_total=$(($check_failed+$check_succeed))
                echo -e "Total attempts: $check_total"
            ;;
            "x")
                exit 0
            ;;
            "X")
                exit 0
            ;;
            *)
                echo "Ooops, wrong command"
            ;;
        esac
        echo -e "\n"
        unset IFS
    done
    }
    ```
    Terdapat `while` loop dengan parameter true untuk infinite loop hingga command `x/X` untuk exit. Setelah menginput command, input akan dipecah menjadi array dengan sintaks `read -ra Arr <<< $input`. `Read -ra` akan menampung hasil input yang sudah dipecah menjadi array dengan delimiter `IFS` ke dalam variabel `Arr`. Setelah dipecah, array dalam variabel `Arr[0]` akan dicek apakah kata tersebut adalah `att` atau `dl`. <br>
    Jika dl, maka hasil array Arr[1] akan ditampung untuk digunakan lebih lanjut. Sebelum mendownload dan men-zip file, ada beberapa konfigurasi yang dilakukan: <br>
    - `init_num=0` -> inisialisasi untuk format nama file gambar (PIC_XX)
    - `dir_date=$(date '+%Y-%m-%d')` -> memformat tanggal agar sesuai soal (YYYY-MM-DD)
    - `path_dir=$dir_date"_$username"` -> sebagai format nama file zip dan folder nantinya 

    Setelah konfigurasi dilakukan, akan dicek apakah file zip sudah ada atau belum. Jika sudah ada, maka file zip akan di-extract dengan passwordnya dan akan ditambahkan gambar sesuai dengan indeks terakhir gambar pada folder tersebut. Jika belum ada, maka buat folder sesuai dengan variabel path_dir.
    ```shell
    FILE=$path_dir".zip"
    if [[ -f "$FILE" ]]
    then
        eval "unzip -P $password $FILE"
        
        init_num=$(eval "ls $path_dir/ | wc -l")
    else
        eval "mkdir $path_dir"
    fi
    ```
    Sintaks `[[ -f  “$FILE” ]]` adalah untuk mengecek apakah ada file di path tersebut. Jika ada, maka akan masuk kedalam if dengan alur sebagai berikut.
    - Men-extract file zip dengan sintaks `eval "unzip -P $password $FILE"`
    - Menghitung berapa banyak file didalam folder hasil extract dengan sintaks `$(eval "ls $path_dir/ | wc -l")` yang hasilnya akan ditampung ke dalam `init_num`

    Jika tidak, maka akan membuat folder baru dengan sintaks `eval "mkdir $path_dir"`

    Tahap selanjutnya adalah men-download gambar sekaligus memformatnya sesuai indeks keberapa gambar tersebut di download
    ```shell
    total=$((init_num+num))
    for ((i=init_num+1; i<=total; i++))
    do
        img_path=$path_dir"/PIC_$i"
        wget_msg="wget -O $img_path https://loremflickr.com/320/240"
        eval $wget_msg
    done
    ```
    Berikut beberapa penjelasan mengenai kode:
    - Variabel `total` sebagai total gambar yang akan ada di dalam folder
    - `for` loop dari indeks terakhir gambar di folder hingga `total` (Jika folder kosong, akan bernilai 0)
    - Variabel `img_path` sebagai path sekaligus format nama gambar
    - `wget_msg` untuk mendownload gambar dari link pada path `-O $img_path`
    - `eval` untuk menjalankan `wget_msg` sebagai command

    Tahap terakhir dari command `dl x` adalah men-zip folder sesuai nama folder dan di-password sesuai dengan password user.
    ```shell
    zip_msg="zip -r -P $password $path_dir $path_dir"
    eval $zip_msg
    eval "rm -rf $path_dir"
    ```
    Berikut penjelasan kode:
    - Sintaks `zip_msg="zip -r -P $password $path_dir $path_dir"` untuk men-zip folder dengan path `$path_dir` lalu dipassword dengan `-P $password` yang akan disimpan ke path yang sama yaitu `$path_dir`
    - `eval “rm -rf $path_dir”` untuk mendelete folder `path_dir` beserta isinya

    Jika hasil `Arr[1]` adalah `att`, maka awk akan digunakan untuk menjumlah attempt login baik yang berhasil maupun tidak yang nantinya akan dijumlah dan di-print ke console
    ```shell
    check_failed=$(awk -v pat="$username" '$NF~pat{++n} END {print n}' log.txt)
    check_succeed=$(awk -v pat="$username" '$6~pat{++n} END {print n}' log.txt)
    check_total=$(($check_failed+$check_succeed))
    echo -e "Total attempts: $check_total"
    ```
    Berikut penjelasan kode:
    - `$NF` adalah untuk field terakhir dari `log.txt`
    - `$6` adalah field keenam dari `log.txt`
    - `-v pat=”$username”` menetapkan nilai `pat` dengan nilai `username`
    - `~pat{++n}` adalah jika mendapatkan kata yang sama dengan `pat`, maka hitung total kemunculannya ke dalam variabel `n`.
    - `END {print n}` mengembalikan hasil `n`


> Berikut adalah output ketika menjalankan command dl N yang menghasilkan zip terproteksi password berisi gambar yang di download

![ss_dln.png](images/ss_dln1.png)
![ss_dln.png](images/ss_dln2.png)

> Berikut adalah output ketika menjalankan command att

![ss_att.png](images/ss_att.png)

<br>

## Soal 2
Pada tanggal 22 Januari 2022, website https://daffa.info di hack oleh seseorang yang tidak bertanggung jawab. Sehingga hari sabtu yang seharusnya hari libur menjadi berantakan. Dapos langsung membuka log website dan menemukan banyak request yang berbahaya.
Bantulah Dapos untuk membaca log website https://daffa.info Buatlah sebuah script awk bernama "soal2_forensic_dapos.sh" untuk melaksanakan tugas-tugas (2a-2e).

## Penyelesaian Soal 2

### 2a
- `Nomor 2a` Membuat folder bernama forensic_log_website_daffainfo_log <br>
    - Menggunakan `-d` untuk mengecek apakah ada direktori folder tersebut, kemudian dengan operator logika `||` (or) untuk kondisi kedua jika folder yang dimaksud tidak ada, maka akan membuat folder direktori tersebut dengan command mkdir. Dituliskan dalam kode berikut:
    ```shell 
    #2a
    [ -d forensic_log_website_daffainfo_log ] || mkdir forensic_log_website_daffainfo_log
    ``` 
    > Berikut adalah hasil dari jawaban 2a:

![ss_file2a.png](images/ss_file2a.png)

### 2b
- `Nomor 2b` Mendapatkan serangan rata-rata request per jam dan disimpan dalam file ratarata.txt <br>
    - Pertama, membuat fungsi `rata2()` yang akan dipanggil untuk menampilkan output dan menyimpannya pada file `ratarata.txt` dalam folder forensic_log_website_daffainfo_log. Dituliskan dalam kode berikut:
    ```shell
    #2b
    rata2(){
        msg="$1"
        rata_msg="$msg"
        echo "$rata_msg" > forensic_log_website_daffainfo_log/ratarata.txt
    }
    ```
    - Selanjutnya membuat variabel `info` lalu dengan awk mengambil data jam-menit-detik (argumen ke 3, 4, dan 5) dari file log_website_daffainfo.log dan membuat variabel `req` untuk menyimpan total request yang dilakukan. Dituliskan dalam kode berikut:
    ```shell
    info=$(awk -F: '
        NR==2{printf "%d %d %d\n", $3, $4, $5}
        END{printf "%d %d %d %d", $3, $4, $5, NR-1}
    ' log_website_daffainfo.log)
    arr=(${info//:/ })
    
    req="${arr[6]}"
    ```
    - Mengubah format jam, menit, dan detik menjadi kedalam satuan jam kemudian totalnya disimpan dalam variabel `total_jam`. Dituliskan dalam kode berikut:
    ```shell
    jam=$(($((${arr[3]}-1))-${arr[0]}))
    menit=$(($((${arr[4]+59}))-${arr[1]}))
    detik=$(($((${arr[5]+60}))-${arr[2]}))
    total_jam=$(($(($detik/3600))+$(($menit/60))+$jam))
    ```
    - Kemudian menghitung rata-rata request perjamnya dengan membagi total request dengan total jam, lalu disimpan dalam variabel `ratarata`. Lalu ditampilkan outputnya dengan format seperti yang diminta pada soal dengan memanggil variabel `ratarata` dan fungsi `rata2`. Dituliskan dalam kode berikut:
    ```shell
    ratarata=$(($req/$total_jam))
    
    msg="Rata-rata serangan adalah sebanyak $ratarata requests per jam"
    rata2 "$msg"
    ```
    > Berikut adalah hasil dari jawaban 2b:

![ss_rata2b.png](images/ss_rata2b.png)

### 2c
- `Nomor 2c` Mendapatkan IP yang paling banyak melakukan request dan disimpan pada file result.txt <br>
    - Pada variabel `info`, dengan menggunakan awk untuk mendapatkan data IP Address dari file log_website_daffainfo.log. Dituliskan dalam kode berikut:
    ```shell
    info=$(awk -F: '
        NR!=1{printf "%s\n", $1}
    ' log_website_daffainfo.log)
    arr=(${info//'"'/ })
    ```
    - Jadi `NR!=1` akan men-skip baris satu yang mengandung data IP, kemudian mengambil argumen pertama yang merupakan IP Address dalam file log_website_daffainfo.log. <br>
    - Lalu `for loop` digunakan untuk menyimpan semua IP Address dan disimpan ke dalam file sementara `temp_log.txt`. Kemudian dengan awk mencari IP Address yang paling sering muncul dari array pada file temp_log.txt, dan outputnya ditampilkan dalam file `result.txt` pada folder forensig_log_website_daffainfo_log sesuai format yang diminta. Dituliskan dalam kode berikut:
    ```shell
    for i in ${arr[@]}
    do
        echo $i >> temp_log.txt
    done
    $(awk '
        {if(arr[$0]++ >= max)
            max = arr[$0]}
        END{for(i in arr) if(max==arr[i])
            printf "IP yang paling banyak mengakses server adalah: %s sebanyak %d requests\n\n",i,arr[i]}
    ' temp_log.txt > forensic_log_website_daffainfo_log/result.txt)
    eval "rm temp_log.txt"
    ```
    - eval "rm temp_log.txt" untuk menghapus file sementara yang tadi digunakan untuk menyimpan IP Address. <br>

### 2d
- `Nomor 2d` Mendapatkan banyak request yang menggunakan user-agent curl dan disimpan pada file result.txt <br>
    - Dengan konsep awk melakukan melakukan perhitungan berapa jumlah baris yang mengandung `curl` dari file log_website_daffainfo.log lalu output ditampilkan dan disimpan dalam file `result.txt` pada folder forensic_log_website_daffainfo_log. Dituliskan dalam kode berikut:
    ```shell
    #2d
    $(awk '
        /curl/ {n++}
        END{printf "Ada %d requests yang menggunakan curl sebagai user-agent\n\n",n}
    ' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt)
    ```

### 2e
- `Nomor 2e` Mendapatkan IP Address yang mengirim request pada jam 2 tanggal 22 dan disimpan dalam file result.txt <br>
    - Dengan konsep awk, jika argumen `$3` yang diambil dari file log_website_daffainfo.log sama dengan `02` (yang menunjukkan jam 2), maka print argumen `$1` yang merupakan IP addressnya lalu disimpan dalam file `result.txt` pada folder forensic. Dituliskan dalam kode berikut:
    ```shell
    #2e
    $(awk -F: '
        {if($3=="02")
            printf "%s\n",$1}
    ' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt)
    ```
    > Berikut adalah hasil dari jawaban 2c, 2d, dan 2e:

![ss_result2cde.png](images/ss_result2cde.png)
![ss_result2e2.png](images/ss_result2e2.png)
![ss_result2e3.png](images/ss_result2e3.png)
- Keterangan: 
    - Line 1 : output dari nomer 2c
    - Line 3 : output dari nomer 2d
    - Line 5-92 : output dari nomer 2e

<br>

## Soal 3
Ubay sangat suka dengan komputernya. Suatu saat komputernya crash secara tiba-tiba :(. Tentu saja Ubay menggunakan linux. Akhirnya Ubay pergi ke tukang servis untuk memperbaiki laptopnya. Setelah selesai servis, ternyata biaya servis sangatlah mahal sehingga ia harus menggunakan dana kenakalannya untuk membayar biaya servis tersebut. Menurut Mas Tukang Servis, laptop Ubay overload sehingga mengakibatkan crash pada laptopnya. Karena tidak ingin hal serupa terulang, Ubay meminta kalian untuk membuat suatu program monitoring resource yang tersedia pada komputer.

Buatlah program monitoring resource pada komputer kalian. Cukup monitoring ram dan monitoring size suatu directory. Untuk ram gunakan command `free -m`. Untuk disk gunakan command `du -sh `. Catat semua metrics yang didapatkan dari hasil `free -m`. Untuk hasil `du -sh ` catat size dari path directory tersebut. Untuk target_path yang akan dimonitor adalah /home/{user}/.

Note:
- nama file untuk script per menit adalah minute_log.sh
- nama file untuk script agregasi per jam adalah aggregate_minutes_to_hourly_log.sh
- semua file log terletak di /home/{user}/log


### Soal 3.a 
Masukkan semua metrics ke dalam suatu file log bernama metrics_{YmdHms}.log. {YmdHms} adalah waktu disaat file script bash kalian dijalankan.

### Pembahasan 3.a
Pertama-tama, kita lihat terlebih dahulu output dari perintah `free -m` saat dijalankan di terminal:

![output_free.png](images/output_free.PNG)

Sedangkan untuk perintah `du -sh` menghasilkan keluaran:

![output_du-sh.png](images/output_du-sh.PNG)

Kemudian membuat program monitoring di script shell `minute_log.sh`, mendeklarasikan variable `mem` dan `disk` diikuti dengan perintah `eval`. 
- Perintah `eval` sendiri yaitu untuk mengevaluasi beberapa perintah/argument
- Variable `mem` akan memonitor ram menggunakan command `free-m` 
- Variable `disk` akan memonitor size pada suatu directory menggunakan command `du-sh` diikuti dengan target path yang akan dimonitoring yaitu `/home/shafira` lalu akan menampilkan nilai dari ram dan size pada target  path tersebut menggunakan perintah `echo`. 
- Variable `log_date` untuk menyimpan format nama dari file nantinya sesuai dengan format metrics_{YmdHms}.log. 
```shell
#!/bin/bash
mem=$(eval "free -m")
disk=$(eval 'du -sh /home/shafira')
echo $mem
echo $disk
log_date=$(date '+%Y%m%d%H%M%S')
````
Setelah itu memanggil nilai dari masing-masing `mem` menggunakan `array` dan `arr` untuk memanggil path size dari `disk` sebelumnya.
```shell
array=(${mem//:/ })
echo "${array[16]}"
mem_total="${array[7]}"
mem_used="${array[8]}"
mem_free="${array[9]}"
mem_shared="${array[10]}"
mem_buff="${array[11]}"
mem_available="${array[12]}"
swap_total="${array[14]}"
swap_used="${array[15]}"
swap_free="${array[16]}"
arr=(${disk//:/ })
```
Kemudian `log_msg` untuk mengembalikan nilai `mem_total, mem_used, mem_free, mem_shared, mem_buff, mem_available, swap_total, swap_used, target path, dan path size` lalu menampilkan nilai dari `log_msg` yang sudah dipanggil tadi.
```shell
log_msg="$mem_total,$mem_used,$mem_free,$mem_shared,$mem_buff,$mem_available,$swap_total,$swap_used,$swap_free,/home/shafira,$arr"
echo $log_msg
```
Setelah itu mengecek apakah directory `/home/shafira/log` sudah ada atau belum, jika sudah ada maka akan langsung memproses `:` namun jika belum ada maka akan membuat directory `/home/shafira/log` menggunakan perintah `mkdir`.
```shell
if [ -d "/home/shafira/log" ]
then
    :
else
    eval "mkdir /home/shafira/log"
fi
```
Kemudian membuat suatu file log yang memiliki format nama `metrics_{YmdHms}.log` dimana di awal tadi kita sudah mendeklarasikan variable `log_date`. Lalu menampilkan `log_msg` pada file log tersebut yang berisi nilai ram dan size disk dan disimpan di directory `/home/shafira/log`.
```shell
filename="metrics_$log_date.log"
echo $log_msg >> /home/shafira/log/$filename
```
Pada akhir script, melakukan pengubahan akses file sehingga hanya user pemilik file yang dapat membaca menggunakan perintah `chmod`
```shell
chmod 700 /home/shafira/log/$filename
```

Berikut merupakan output dari soal 3a:

![3a.png](images/3a.PNG)

![3a1.png](images/3a1.PNG)

![3a2.png](images/3a2.PNG)


### Soal 3.b
Script untuk mencatat metrics diatas diharapkan dapat berjalan otomatis pada setiap menit.

### Pembahasan 3.b
Karena script `minute_log.sh` yang diminta harus berjalan otomatis pada tiap menit, maka kita dapat menggunakan perintah `cron`. Masukkan perintah `crontab -e` untuk membuat crontab baru, lalu pada bagian bawah script crontab, kita tulis `* * * * * bash /home/shafira/sisop/modul1/minute_log.sh` yang berarti jalankan script shell tersebut tiap menitnya. Script tersebut kemudian akan menghasilkan file .log berdasarkan nama metrics_{YmdHms}.log yang berisi nilai memori dan disk setiap menitnya.

Berikut merupakan output dari soal 3b:

![3b.png](images/3b.PNG)


### Soal 3.c
Buat satu script untuk membuat agregasi file log ke satuan jam. Script agregasi akan memiliki info dari file-file yang tergenerate tiap menit. Dalam hasil file agregasi tersebut, terdapat nilai minimum, maximum, dan rata-rata dari tiap-tiap metrics. File agregasi akan ditrigger untuk dijalankan setiap jam secara otomatis. 
### Pembahasan 3.c
Membuat script shell dengan nama `aggregate_minutes_to_hourly_log.sh`. Kemudian mendeklarasikan variable `log_date` untuk menyesuaikan nama file dengan format metrics_agg_{YmdH}.log yang akan tersimpan di output dan dirloc sebagai tempat directory location.
```shell
#!/bin/bash
 
log_date=$(date '+%Y%m%d%H')
output="/home/shafira/log/metrics_agg_$log_date.log"
dirloc="/home/shafira/"
```
Kemudian membuat fungsi `get_file()` untuk mendapatkan data dari file log yang dihasilkan setiap menit sebelumnya.
```shell
get_file() {
    for file in $(ls /home/shafira/log/metrics_2022* | grep $log_date)
    do
        cat $file | grep -v mem
    done
}
``` 
- Perintah `ls /home/shafira/log/metrics_2022*` untuk mendapatkan semua file dengan awalan metrics_2022* pada `/home/shafira/log` lalu print file yang terdapat sesuai dengan `$log_date` pada jam saat itu.
- Di dalam perulangan tersebut, terdapat perintah `cat $file | grep -v mem` untuk membaca setiap satuan filenya, tetapi hanya print baris yang tidak terdapat kata “mem”.

Kemudian membuat fungsi `get_list()` untuk memproses setiap data yang sudah diambil pada fungsi `get_file()` yang mana akan melakukan print sesuai data minimum, maximum, atau average setiap data per-kategorinya. Keluaran dari fungsi tersebut adalah sebuah angka yang nantinya dapat disimpan pada sebuah variable.

- Melakukan pengkondisian terlebih dahulu berdasarkan argumen pertamanya yang mana akan menentukan jenis data apa yang akan diproses
```shell
if [ $1 == "mem_total" ]
    then
        x=1
    elif [ $1 == "mem_used" ]
    then
        x=2
    elif [ $1 == "mem_free" ]
    then
        x=3
    elif [ $1 == "mem_shared" ]
    then
        x=4
    elif [ $1 == "mem_buff" ]
    then
        x=5
    elif [ $1 == "mem_available" ]
    then
        x=6
    elif [ $1 == "swap_total" ]
    then
        x=7
    elif [ $1 == "swap_used" ]
    then
        x=8
    elif [ $1 == "swap_free" ]
    then
        x=9
    elif [ $1 == "path_size" ]
    then
        x=11
    fi
```
- Perintah `AWK` untuk memproses datanya. Sebelum masuk ke `AWK`, terlebih dahulu memanggil 2 buah variable, `indx` sesuai dengan nilai `$x`, serta pilihan melalui variable `choose` sesuai dengan argumen kedua pemanggilan fungsi. Di dalam `awk` tersebut, melakukan pengkondisian sesuai nilai `$indx`. Diakhir pengkondisian akan selalu di lakukan operasi penjumlahan variable total berdasarkan `$indx`, dan variable `count` berdasarkan banyaknya perulangan yang terjadi.
```shell
get_file | awk -F , '
    {
        if(min == "") {
            min=max=$indx
        }
        if($indx > max) {
            max=$indx
        }
        if($indx < min) {
            min=$indx
        }
       
        sum+=$indx;
        count+=1
 
    } END {
        if(choose == "max") {
            print max
        }
        if(choose == "min") {
            print min
        }
        if(choose == "avg") {
            average=sum/count
            printf "%.3f", average
        }
    }' indx=$x choose=$2
}
```
Kemudian untuk mendapatkan data-data yang sesuai, kita tinggal memanggil `fungsi get_list()` dan memanggil argumen yang sesuai.
```shell
mem_total_min=$(get_list mem_total min)
mem_total_max=$(get_list mem_total max)
mem_total_avg=$(get_list mem_total avg)
 
#mem_used
mem_used_min=$(get_list mem_used min)
mem_used_max=$(get_list mem_used max)
mem_used_avg=$(get_list mem_used avg)
 
#mem_free
mem_free_min=$(get_list mem_free min)
mem_free_max=$(get_list mem_free max)
mem_free_avg=$(get_list mem_free avg)
 
#mem_shared
mem_shared_min=$(get_list mem_shared min)
mem_shared_max=$(get_list mem_shared max)
mem_shared_avg=$(get_list mem_shared avg)
 
#mem_buff
mem_buff_min=$(get_list mem_buff min)
mem_buff_max=$(get_list mem_buff max)
mem_buff_avg=$(get_list mem_buff avg)
 
#mem_available
mem_available_min=$(get_list mem_available min)
mem_available_max=$(get_list mem_available max)
mem_available_avg=$(get_list mem_available avg)
 
#swap_total
swap_total_min=$(get_list swap_total min)
swap_total_max=$(get_list swap_total max)
swap_total_avg=$(get_list swap_total avg)
 
#swap_used
swap_used_min=$(get_list swap_used min)
swap_used_max=$(get_list swap_used max)
swap_used_avg=$(get_list swap_used avg)
 
#swap_free
swap_free_min=$(get_list swap_free min)
swap_free_max=$(get_list swap_free max)
swap_free_avg=$(get_list swap_free avg)
 
#path_size
path_size_min=$(get_list path_size min)
path_size_max=$(get_list path_size max)
path_size_avg=$(get_list path_size avg)
```
 
Setelah itu, print semua variable diatas lalu simpan pada `$output`
```shell
echo "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" >> $output
echo "minimum,$mem_total_min,$mem_used_min,$mem_free_min,$mem_shared_min,$mem_buff_min,$mem_available_min,$swap_total_min,$swap_used_min,$swap_free_min,$dirloc,$path_size_min" >> $output
echo "maximum,$mem_total_max,$mem_used_max,$mem_free_max,$mem_shared_max,$mem_buff_max,$mem_available_max,$swap_total_max,$swap_used_max,$swap_free_max,$dirloc,$path_size_max" >> $output
echo "average,$mem_total_avg,$mem_used_avg,$mem_free_avg,$mem_shared_avg,$mem_buff_avg,$mem_available_avg,$swap_total_avg,$swap_used_avg,$swap_free_avg,$dirloc,$path_size_avg" >> $output
```

Di akhir script, tidak lupa untuk mengubah perizinan file log agar hanya user pemilik file yang bisa membaca
```shell
chmod 700 $output
```

Berikut merupakan output dari soal 3c:

![3c.png](images/3c.PNG)

### Soal 3.d
Karena file log bersifat sensitif pastikan semua file log hanya dapat dibaca oleh user pemilik file.

### Pembahasan 3.d
Agar semua file log hanya dapat dibaca oleh user, kita dapat menjalankan perintah `chmod 700 $output`
- Perintah `chmod` untuk mengubah perizinan user
- Angka `7` agar user dapat melakukan read, write, dan execute pada file
- Angka `0` kedua agar grup user tidak memiliki akses terhadap file
- Angka `0` ketiga agar orang lain tidak memiliki akses terhadap file

Berikut merupakan output dari soal 3d:

![3d.png](images/3d.PNG)
