#!/bin/bash

log_date=$(date '+%Y%m%d%H')
output="/home/shafira/log/metrics_agg_$date_time.log"
dirloc="/home/shafira/"

get_file() {
	for file in $(ls /home/shafira/log/metrics_2022* | grep $log_date)
	do 
		cat $file | grep -v mem 
	done
}

get_list() {
	if [ $1 == "mem_total" ]
	then
		x=1
	elif [ $1 == "mem_used" ]
	then
		x=2
	elif [ $1 == "mem_free" ]
	then
		x=3
	elif [ $1 == "mem_shared" ]
	then
		x=4
	elif [ $1 == "mem_buff" ]
	then
		x=5
	elif [ $1 == "mem_available" ]
	then
		x=6
	elif [ $1 == "swap_total" ]
	then
		x=7
	elif [ $1 == "swap_used" ]
	then
		x=8
	elif [ $1 == "swap_free" ]
	then
		x=9
	elif [ $1 == "path_size" ]
	then
		x=11
	fi

	get_file | awk -F , '
	{
		if(min == "") {
			min=max=$indx
		}
		if($indx > max) {
			max=$indx
		}
		if($indx < min) {
			min=$indx
		}
		
		sum+=$indx;
		count+=1

	} END {
		if(choose == "max") {
			print max
		}
		if(choose == "min") {
			print min
		}
		if(choose == "avg") {
			average=sum/count
			printf "%.3f", average
		}
	}' indx=$x choose=$2
}


mem_total_min=$(get_list mem_total min)
mem_total_max=$(get_list mem_total max)
mem_total_avg=$(get_list mem_total avg)

#mem_used
mem_used_min=$(get_list mem_used min)
mem_used_max=$(get_list mem_used max)
mem_used_avg=$(get_list mem_used avg)

#mem_free
mem_free_min=$(get_list mem_free min)
mem_free_max=$(get_list mem_free max)
mem_free_avg=$(get_list mem_free avg)

#mem_shared
mem_shared_min=$(get_list mem_shared min)
mem_shared_max=$(get_list mem_shared max)
mem_shared_avg=$(get_list mem_shared avg)

#mem_buff
mem_buff_min=$(get_list mem_buff min)
mem_buff_max=$(get_list mem_buff max)
mem_buff_avg=$(get_list mem_buff avg)

#mem_available
mem_available_min=$(get_list mem_available min)
mem_available_max=$(get_list mem_available max)
mem_available_avg=$(get_list mem_available avg)

#swap_total
swap_total_min=$(get_list swap_total min)
swap_total_max=$(get_list swap_total max)
swap_total_avg=$(get_list swap_total avg)

#swap_used
swap_used_min=$(get_list swap_used min)
swap_used_max=$(get_list swap_used max)
swap_used_avg=$(get_list swap_used avg)

#swap_free
swap_free_min=$(get_list swap_free min)
swap_free_max=$(get_list swap_free max)
swap_free_avg=$(get_list swap_free avg)

#path_size
path_size_min=$(get_list path_size min)
path_size_max=$(get_list path_size max)
path_size_avg=$(get_list path_size avg)

echo "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" >> $output
echo "minimum,$mem_total_min,$mem_used_min,$mem_free_min,$mem_shared_min,$mem_buff_min,$mem_available_min,$swap_total_min,$swap_used_min,$swap_free_min,$dirloc,$path_size_min" >> $output
echo "maximum,$mem_total_max,$mem_used_max,$mem_free_max,$mem_shared_max,$mem_buff_max,$mem_available_max,$swap_total_max,$swap_used_max,$swap_free_max,$dirloc,$path_size_max" >> $output
echo "average,$mem_total_avg,$mem_used_avg,$mem_free_avg,$mem_shared_avg,$mem_buff_avg,$mem_available_avg,$swap_total_avg,$swap_used_avg,$swap_free_avg,$dirloc,$path_size_avg" >> $output

chmod 700 $output

#cronjob
# * * * * * bash /home/shafira/sisop/modul1/aggregate_minutes_to_hourly_log.sh


	
