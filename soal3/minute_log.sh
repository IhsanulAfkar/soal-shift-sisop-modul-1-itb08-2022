#!/bin/bash

mem=$(eval "free -m")
disk=$(eval 'du -sh /home/shafira')
echo $mem
echo $disk
log_date=$(date '+%Y%m%d%H%M%S')

array=(${mem//:/ })

echo "${array[16]}"
mem_total="${array[7]}"
mem_used="${array[8]}"
mem_free="${array[9]}"
mem_shared="${array[10]}"
mem_buff="${array[11]}"
mem_available="${array[12]}"
swap_total="${array[14]}"
swap_used="${array[15]}"
swap_free="${array[16]}"

arr=(${disk//:/ })

log_msg="$mem_total,$mem_used,$mem_free,$mem_shared,$mem_buff,$mem_available,$swap_total,$swap_used,$swap_free,/home/shafira,$arr"
echo $log_msg

if [ -d "/home/shafira/log" ]
then
	:
else
	eval "mkdir /home/shafira/log"
fi
filename="metrics_$log_date.log"
echo $log_msg >> /home/shafira/log/$filename

chmod 700 /home/shafira/log/$filename

#cronjob
# * * * * * bash /home/shafira/sisop/modul1/minute_log.sh
