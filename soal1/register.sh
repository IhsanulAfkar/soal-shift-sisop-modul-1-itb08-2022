read -p "Enter username: " username
read -s -p "Enter password: " password
echo -e "\n"
log_date=$(date '+%m/%d/%Y %H:%M:%S')
invalid="password is invalid"

log(){
    msg="$1"
    log_msg="$log_date $msg"
    echo "$log_msg" >> log.txt
}

regist(){
    echo "$1 $2" >> users/user.txt
    log "INFO User $1 registered successfully"
}

validate(){
    username="$1"
    password="$2"
    # check if password is same with username
    if [ $password == $username ]
    then
        echo $invalid
        return 0
    # check if password is 8 characters or greater
    elif [ ${#password} -lt 8 ] 
    then
        echo $invalid
        return 0 
    fi  
    number=0
    uppercase=0
    lowercase=0

    #Check if have uppercase, lowercase, and alphanumeric
    for ((i=0; i<${#password}; i=i+1))
    do
        c="${password:$i:1}" 
        if [[ "$c" =~ [A-Z] ]]
        then
                ((uppercase++))
        elif [[ "$c" =~ [a-z] ]]
        then
                ((lowercase++))
        elif [[ "$c" =~ [0-9] ]]
        then
                ((number++)) 
        fi
    done
    if [ $uppercase == 0 ]
    then
        echo $invalid
        return 0
    elif [ $lowercase == 0 ]
    then 
        echo $invalid
        return 0
    elif [ $number == 0 ]
    then
        echo $invalid
        return 0
    fi

    # Check if username alreadi exist
    check=$(awk -v pat="$username" '$1~pat{print $1}' users/user.txt)
    if [ -n "$check" ]
    then
        echo $invalid
        msg="REGISTER: ERROR User already exists"
        log "$msg"
        return 0
    fi

    # Username and Password will be registered
    regist "$username" "$password"
}
validate "$username" "$password"
