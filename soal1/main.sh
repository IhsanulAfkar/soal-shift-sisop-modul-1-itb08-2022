read -p "Enter username: " username
read -s -p "Enter password: " password
echo -e "\n"
log_date=$(date '+%m/%d/%Y %H:%M:%S')
invalid="wrong password or username"

log(){
    msg="$1"
    log_msg="$log_date $msg"
    echo "$log_msg" >> log.txt
}

login(){
	username="$1"
	password="$2"
	while [ 1 ]
	do
		echo "Enter x/X to exit (or ^C)"
		read -p "Enter command: " input
		echo -e "\n"
		IFS=' '
		read -ra Arr <<< "$input"
		command="${Arr[0]}"
		case "$command" in
			"dl")
				num="${Arr[1]}"
				init_num=0;
				dir_date=$(date '+%Y-%m-%d')
				path_dir=$dir_date"_$username"
				FILE=$path_dir".zip"
				if [[ -f "$FILE" ]]
				then
					eval "unzip -P $password $FILE"
					
					init_num=$(eval "ls $path_dir/ | wc -l")
				else
					eval "mkdir $path_dir"
				fi
				total=$((init_num+num))
				for ((i=init_num+1; i<=total; i++))
				do
					img_path=$path_dir"/PIC_$i"
					wget_msg="wget -O $img_path https://loremflickr.com/320/240"
					eval $wget_msg
				done
				zip_msg="zip -r -P $password $path_dir $path_dir"
				eval $zip_msg
				eval "rm -rf $path_dir"
			;;
			"att")
				check_failed=$(awk -v pat="$username" '$NF~pat{++n} END {print n}' log.txt)
				check_succeed=$(awk -v pat="$username" '$6~pat{++n} END {print n}' log.txt)
				check_total=$(($check_failed+$check_succeed))
				echo -e "Total attempts: $check_total"
			;;
			"x")
				exit 0
			;;
			"X")
				exit 0
			;;
			*)
				echo "Ooops, wrong command"
			;;
		esac
		echo -e "\n"
		unset IFS
	done
}

validate(){
	username="$1"
	password="$2"
	# Check if exist or not
	check=$(awk -v pat="$username" '$1~pat{print $2}' users/user.txt)
	if [ -z "$check" ]
	then
		echo $invalid
		return 0
	fi
	# Check password 
	if [ "$check" != "$password" ]
	then
		echo $invalid
		msg="LOGIN: ERROR Failed login attempt on user $username"
		log "$msg"
		return 0
	fi
	# Logged in
	msg="LOGIN: INFO User $username logged in"
	log "$msg"
	login "$username" "$password"
}

validate "$username" "$password"
 
